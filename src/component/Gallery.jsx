import React from 'react'
import Profile from './Profile'

function Gallery() {
  return (
    <section>
        <h1> This is first Gallery</h1>
        <Profile size={30} person={{name:'hello'}} ></Profile>
        <Profile size={50}></Profile>
        <Profile size={80}></Profile>
    </section>
  )
}

export default Gallery;