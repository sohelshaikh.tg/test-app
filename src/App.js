import './App.css';
import { Gallery, Profile, Avatar } from './component';

function App() {
  const person = {
    name: 'Gregorio Y. Zara',
    name1: 'Gregorio Y. Zara1',
    name2: 'Gregorio Y. Zara2',
    theme: {
      backgroundColor: 'black',
      color: 'pink'
    }
  };
  return (
    <div className="App" style={person.theme}>
      <Gallery></Gallery>
    </div>
  );
}

export default App;


